from django.urls import path

from .views import Inicio, DetailView, BlogView, ProyectoView, CategoriaView, AboutView,ContactView, SuscribeView,GalleryView
from . import views

urlpatterns = [
    path("", Inicio.as_view(), name="home"),    
    path("blog/", BlogView.as_view(), name="blog"),
    path("projects/", ProyectoView.as_view(), name="projects"),    
    path("about-me/", AboutView.as_view(), name="about-me"),
    path("contact-me/", ContactView.as_view(), name="contact-me"),
    path("category/<str:name_cate>/", CategoriaView.as_view(), name="category"),
    path("detail/<str:slug>", DetailView.as_view(), name="detail"),    
    path("search/", views.buscar, name="search"),
    path("suscribe/",SuscribeView.as_view(),name="suscribe"),
    path("gallery/",GalleryView.as_view(),name="gallery"),

]

