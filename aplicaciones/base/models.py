from django.db import models
from ckeditor.fields import RichTextField

from cloudinary.models import CloudinaryField
from django.db.models.signals import pre_delete

from django.dispatch import receiver

import cloudinary
import cloudinary.uploader
import cloudinary.api


class ModeloBase(models.Model):
    id=models.AutoField(primary_key=True)
    estado=models.BooleanField('estado',default=True)
    fecha_creacion=models.DateField('fecha creacion',auto_now= False ,auto_now_add=True)
    fecha_modificacion=models.DateField("fecha modificacion", auto_now=True, auto_now_add=False)
    fecha_eliminacion=models.DateField("fecha eliminacion", auto_now=True, auto_now_add=False)


    class Meta:
        abstract=True

class Autor(ModeloBase):
    nombre = models.CharField("nombre", max_length=100)
    apellido = models.CharField("apellido", max_length=150)
    email = models.EmailField("email", max_length=254)
    descripcion =models.TextField("descripcion", blank=True, null=True)
    image_referencia =CloudinaryField('image avatar',folder='blog/avatar/')
    image_background =CloudinaryField('image background',folder='blog/avatar/')    
    web = models.CharField("web",blank=True, null= True, max_length=150)
    instagram = models.CharField("instagram",blank=True, null= True, max_length=150)
    facebook = models.CharField("facebook",blank=True, null= True, max_length=150)
    twitter = models.CharField("twitter",blank=True, null= True, max_length=150)


    class Meta:
        verbose_name = "autor"
        verbose_name_plural = "autores"
    
    def __str__(self):
        return '{0},{1}'.format(self.nombre,self.apellido)

class Categoria(ModeloBase):
    nombre = models.CharField("nombre", max_length=100)
    image_referencia =CloudinaryField('image referencia',folder='blog/categoria/')

    class Meta:
        verbose_name = "categoria"
        verbose_name_plural = "categorias"

    def __str__(self):
        return self.nombre

class Post(ModeloBase):
    titulo = models.CharField("titulo", max_length=150, unique=True)
    slug = models.CharField("slug", max_length=100, unique=True)
    descripcion = models.TextField("descripcion")
    autor = models.ForeignKey(Autor, on_delete = models.CASCADE)
    categoria = models.ForeignKey(Categoria, on_delete = models.CASCADE,related_name="posts")
    image_referencia =CloudinaryField('image referencia',folder='blog/post/')
    publicado = models.BooleanField("publicado / no publicado",default=False)
    fecha_publicacion= models.DateField("fecha de publicacion")
    contenido = RichTextField() 

    class Meta:
        verbose_name = "post"
        verbose_name_plural = "posts"
    
    def __str__(self):
        return self.titulo
    
    
class RedesSociales(ModeloBase):
    nombre=models.CharField("nombre del sitio web",max_length=200)
    descripcion=models.CharField("descripcion del sitio web",max_length=200)
    facebook = models.CharField("facebook", max_length=200,blank=True, null=True)
    twitter = models.CharField("twitter", max_length=200,blank=True, null=True)
    instagram = models.CharField("instagram", max_length=200,blank=True, null=True)
    email= models.CharField("email", max_length=200,blank=True, null=True)
    url=models.URLField("web", max_length=200 ,blank=True, null=True)
    youtube = models.CharField("youtube", max_length=200,blank=True, null=True)
    gitlab=models.CharField("gitlab", max_length=200 ,blank=True, null=True)
    

    class Meta:
        verbose_name = "redes social"
        verbose_name_plural = "redes sociales"

    def __str__(self):
        return self.facebook
    
class Contacto(ModeloBase):
    nombre = models.CharField("nombre", max_length=100)    
    apellido = models.CharField("apellido", max_length=150)
    correo = models.EmailField("correo", max_length=254)
    asunto = models.CharField("asunto", max_length=200)
    mensage = models.TextField("mensaje")

    class Meta:
        verbose_name = "contacto"
        verbose_name_plural = "contactos"

    def __str__(self):
        return '{0},{1}'.format(self.nombre,self.asunto)
    

class Suscriptores(ModeloBase):
    correo = models.EmailField("correo", max_length=254)

    def __str__(self):
        return self.correo
    
class Galeria(ModeloBase):
    nombre=models.CharField("nombre", max_length=50)
    avatar = CloudinaryField("image",folder="gallery/")
    
    def __str__(self):
        return self.nombre

@receiver(pre_delete, sender=Galeria)
def galeria_delete(sender, instance, **kwargs):
    print(instance.avatar.public_id)
    cloudinary.uploader.destroy(instance.avatar.public_id)