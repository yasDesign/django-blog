from django.contrib import admin
from .models import *
# Register your models here.

from import_export import resources
from import_export.admin import ImportExportModelAdmin

class CategoriaResources(resources.ModelResource):
    class Meta:
        model=Categoria

class CategoriaAdmin(ImportExportModelAdmin):
    list_display=('nombre','estado','fecha_creacion')
    search_fields=['nombre']
    resource_class=CategoriaResources

class AutorResources(resources.ModelResource):
    class Meta:
        model=Autor

class AutorAdmin(ImportExportModelAdmin):
    list_display=('nombre','estado','fecha_creacion')
    search_fields=['nombre']
    resource_class=AutorResources

class PostResources(resources.ModelResource):
    class Meta:
        model=Post

class PostAdmin(ImportExportModelAdmin):
    list_display=('titulo','categoria','publicado','fecha_publicacion')
    search_fields=['titulo']
    resource_class=PostResources

class RedesSocialesResources(resources.ModelResource):
    class Meta:
        model=RedesSociales

class RedesSocialesAdmin(ImportExportModelAdmin):
    list_display=('nombre','facebook','fecha_creacion')
    search_fields=['nombre']
    resource_class=RedesSocialesResources

class SuscriptoresResources(resources.ModelResource):
    class Meta:
        model=Suscriptores

class SuscriptoresAdmin(ImportExportModelAdmin):
    list_display=('correo','fecha_creacion')
    search_fields=['correo']
    resource_class=SuscriptoresResources


class ContactoResources(resources.ModelResource):
    class Meta:
        model=Contacto

class ContactoAdmin(ImportExportModelAdmin):
    list_display=('correo','apellido','correo','fecha_creacion')
    search_fields=['correo']
    resource_class=ContactoResources

class GaleriaResources(resources.ModelResource):
    class Meta:
        model=Galeria

class GaleriaAdmin(ImportExportModelAdmin):
    list_display=('nombre','fecha_creacion')
    search_fields=['nombre']
    resource_class=GaleriaResources

admin.site.register(Autor,AutorAdmin)
admin.site.register(Categoria,CategoriaAdmin)
admin.site.register(Post,PostAdmin)
admin.site.register(RedesSociales,RedesSocialesAdmin)
admin.site.register(Suscriptores,SuscriptoresAdmin)
admin.site.register(Contacto,ContactoAdmin)
admin.site.register(Galeria,GaleriaAdmin)

