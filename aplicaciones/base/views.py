from django.shortcuts import render,redirect
from django.views.generic import ListView,View
from django.db import models
import random
from django.core.paginator import Paginator

from .models import Post, Categoria, Autor, RedesSociales, Suscriptores, Galeria
from blogYas.settings import EMAIL_HOST_USER
from django.db.models.functions import TruncMonth

from .forms import ContactForm
from django.core.mail import send_mail

from django.shortcuts import get_object_or_404



#metodos utils
def getCountByCategories():
    categorias = Categoria.objects.annotate(post_count = models.Count('posts')).order_by('-post_count')
    return categorias

def getRandomPost():
    sub_items=[]
    if Post.objects.count() >= 4:
        item_ids=list(Post.objects.filter(estado=True,publicado=True).values_list('id',flat=True))
        sub_ids=[]
        for x in range(0, 4):
            valor=random.choice(item_ids)
            sub_ids.append(valor)
            item_ids.remove(valor)
    
        for item in sub_ids:
            sub_items.append(Post.objects.get(id=item))
        
    else: 
        sub_items=Post.objects.filter(estado=True,publicado=True).order_by('-fecha_publicacion')
        
    return sub_items

def getDataByMonth():
    posts=[]
    if Post.objects.count() >= 1:
       posts=Post.objects.annotate(month=TruncMonth('fecha_publicacion')).order_by('-month') .values('month').annotate(count=models.Count('id'))  
    
    return posts

# Create your views here.
class Inicio(ListView):

    def get(self,request,*args,**kwargs):
        posts=getRandomPost()
        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'posts':posts,
            'categorias':getCountByCategories(),
            'postsRandom':posts,
            'postsByDate':getDataByMonth(),
            }
        return render(request,'home.html',contexto)

class Detalle(ListView):

    def get(self,request,*args,**kwargs):
        return render(request,'details.html')

class BlogView(ListView):
    
    def get(self,request,*args,**kwargs):
        
        posts=Post.objects.filter(estado=True,publicado=True).order_by('-id')

        paginator=Paginator(posts,6)
        pagina=request.GET.get('page')
        posts=paginator.get_page(pagina)
        
        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'posts':posts,
            'categorias':getCountByCategories(),
            'postsRandom':posts,
            'postsByDate':getDataByMonth(),
        }
        return render(request,'blog.html',contexto)

class ProyectoView(ListView):
    
    def get(self,request,*args,**kwargs):
        category={}
        posts=[]
        mensaje=""
        if Categoria.objects.filter(nombre='proyectos').count() > 0:
            category=Categoria.objects.get(nombre='proyectos')
            posts=Post.objects.filter(categoria=category,estado=True,publicado=True).order_by('-id')
        else:
            mensaje="No existen proyectos todavia"
                
        paginator=Paginator(posts,6)
        pagina=request.GET.get('page')
        posts=paginator.get_page(pagina)

        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'categoria':category,
            'posts':posts,
            'mensaje':mensaje,
            'categorias':getCountByCategories(),
            'postsRandom':getRandomPost(),
            'postsByDate':getDataByMonth(),
        }
        return render(request,'projects.html',contexto)

class CategoriaView(ListView):
    
    def get(self,request,*args,**kwargs):
        cario=kwargs.get('name_cate')
        Categoria.objects.get(nombre=cario,estado=True)    
        category=get_object_or_404(Categoria,nombre=cario,estado=True)

        posts=Post.objects.filter(categoria=category,estado=True,publicado=True).order_by('-id')
                
        paginator=Paginator(posts,6)
        pagina=request.GET.get('page')
        posts=paginator.get_page(pagina)

        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'categoria':category,
            'posts':posts,
            'categorias':getCountByCategories(),
            'postsRandom':getRandomPost(),
            'postsByDate':getDataByMonth(),
        }
        return render(request,'blog.html',contexto)

class DetailView(ListView):
    
    def get(self,request,*args,**kwargs):
        slug_post=kwargs.get('slug')
        ##post=Post.objects.get(slug=slug_post)
        post=get_object_or_404(Post,slug=slug_post)
        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'post':post,
            'categorias':getCountByCategories(),
            'postsRandom':getRandomPost(),
            'postsByDate':getDataByMonth(),
        }
        return render(request,'detail.html',contexto)

class AboutView(ListView):
    
    def get(self,request,*args,**kwargs):
        autor=Autor.objects.all()
        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'autor':autor[0]
        }
        return render(request,'about-me.html',contexto)

class ContactView(View):
    
    def get(self,request,*args,**kwargs):
        form=ContactForm()        
        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'form':form,        
        }
        return render(request,'contact-me.html',contexto)

    def post(self,request,*args,**kwargs):
        form=ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('inicio')
        else:
            contexto={'form':form}
            return render(request,'contact-me')

def buscar(request):
    
    if request.GET.get('q'):
        q=request.GET.get('q')
        posts=Post.objects.filter(titulo__contains=q,estado=True,publicado=True).order_by('-fecha_publicacion')
        print(posts)
        mensaje=""
        if posts.count() == 0:
            mensaje="no se encontraron coincidencias"

        redesSociales=RedesSociales.objects.all()
        contexto={
            'redesSociales':redesSociales[0],
            'posts':posts,
            'mensaje':mensaje,
            'categorias':getCountByCategories(),
            'postsRandom':getRandomPost(),
            'postsByDate':getDataByMonth(),
        }

    return render(request,"blog.html",contexto)

class SuscribeView(View):

    def post(self,request,*args,**kwargs):
        correo=request.POST.get('correo')
        Suscriptores.objects.create(correo=correo)
        
        asunto="Suscription a yaspv.com"
        mensaje="gracias por tu susucripcion a mi blog !!!"
        remitente=EMAIL_HOST_USER
        try:
            send_mail(asunto,mensaje,remitente,[correo])
        except:
            pass


        print(correo)
        return redirect('home')

class GalleryView(ListView):

    def get(self,request,*args, **kwargs):
        object_list=[]
        object_list=Galeria.objects.all()
        contexto={
            'object_list':object_list,
        }
        return render(request,'gallery.html',contexto)