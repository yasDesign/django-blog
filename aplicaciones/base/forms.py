from django import forms
from .models import Contacto

class ContactForm(forms.ModelForm):
    class Meta:
        model=Contacto
        fields='__all__'
        exclude= ('estado',)

        widgets={
            'nombre':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'your name',
            }),
            'apellido':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'your lastname',
            }),
            'correo':forms.EmailInput(attrs={
                'class':'form-control',
                'placeholder':'your email',
            }),
            'asunto':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'subject',
            }),
            'mensage':forms.Textarea(attrs={
                'class':'form-control',
                'placeholder':'message'
            }),
        }
    
   